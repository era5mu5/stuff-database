<?php

// Connect to database
// location, username, password, database name
try {
    $dbc = new PDO('mysql:host=localhost;dbname=stuff', 'root', '');
    $dbc->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (Exception $e) {
    echo "Whoops! ", $e->getMessage();
    die();
}

// Prepare some SQL to read all the users in the users table
$sql = "SELECT Username, Email, FirstName, LastName FROM users 
    WHERE Access = :access";

// Run the query
try {
    $statement = $dbc->prepare($sql);
    $statement->bindValue(":access", $_GET['access']);
    $statement->execute();
} catch (Exception $e) {
    echo "whoopsie " , $e->getMessage();
    die();
}

//Loop through the result
?>
<?php while( $row = $statement->fetch(PDO::FETCH_ASSOC) ) : ?>
    <p>
    <?= $row['FirstName'] ?> <?= $row['LastName'] ?>
     has the username of <?= $row['Username'] ?>
     and can be contacted via <?= $row['Email'] ?>
    </p>
<?php endwhile; ?>