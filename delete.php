<?php

// Connect to database
$dbc = new mysqli('localhost', 'root', '', 'stuff');

if ($dbc->connect_errno) {
    printf("Connect failed: %s\n", $dbc->connect_error);
    exit();
}


// If the user has submitted the form
if( isset($_POST['username']) ) {

    $username = $_POST['username'];

    // Prepare the SQL
    $sql = "DELETE FROM users WHERE Username = '$username' ";

    $result = $dbc->query($sql);

    if (!$result) {
         printf("Error message: %s\n", $dbc->error);
         exit();
    }   

}


// Get all the usernames from the database
$sql = "SELECT Username FROM users";

// Run the query
$result = $dbc->query( $sql );

?>

<h1>Delete an account</h1>
<form action="delete.php" method="post">
    <div>
        <label>Username: </label>
        <select name="username">
            <?php
                // Loop through all the results
                while( $row = $result->fetch_assoc() ) {
                    echo '<option>'.$row['Username'].'</option>';
                }
            ?>
        </select>
        <input type="submit" value="DELETE">
    </div>
</form>
